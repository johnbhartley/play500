const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const path = require('path');
const config = require('./config.js');

const visitorsData = {};

app.set('port', (process.env.PORT || 5000));

app.use(express.static(path.join(__dirname, 'public/')));

app.get(/\/(about|contact)?$/, (req, res) => {
    res.sendFile(path.join(__dirname, 'views/index.html'));
});

app.get('/dashboard', (req, res) => {
    res.sendFile(path.join(__dirname, 'views/dashboard.html'));
});

io.on('connection', (socket) => {
    
    console.log(socket.id + 'connected');
    io.emit('hello', socket.id);

    socket.on('visitor-data', (data) => {
        console.log(data);
        visitorsData[socket.id] = data;
    });

    socket.on('disconnect', () => {
        // a user has left our page - remove them from the visitorsData object
        console.log('so and so disconnected');
        io.emit('goodbye', socket.id);
        delete visitorsData[socket.id];
    });

    socket.on('notes', (data) => {
        io.emit('notes', data);
    })

    console.log(visitorsData);
});

http.listen(app.get('port'), () => {
    console.log('listening on ' + app.get('port'));
});